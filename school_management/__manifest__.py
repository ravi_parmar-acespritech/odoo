# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'school management',
    'version' : '14.1.1',
    'summary': 'school management model',
    'sequence': 10,
    'description': 'School management system is an information system to manage school related data and transactions',
    'category': 'school management',
    'website': 'https://acespritech.com/',
    'depends' : ['mail','sale_management','base','stock'],
    'data':[
        'security/ir.model.access.csv',
        'wizard/school.wizard.xml',
        'views/view_student.xml',
        'views/sale_order_inheritance.xml',
        'views/views_subject.xml',
        'views/account_invoice.xml',
        'views/account_invoice_line.xml',
        'views/tax_views.xml',
        'views/student_attendance.xml',
    ],

    'author': 'ravi',
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'OPL-1',
}






