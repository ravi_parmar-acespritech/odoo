# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields

class AccountInvoice(models.Model):
    _name ='account.invoice'
    _description = 'Account Information'
    _rec_name = 'name'

    name= fields.Char(string='Name')
    partner_name = fields.Many2one('res.partner',string='Partner')
    invoice_lines_ids = fields.One2many('account.invoice.line','invoice_id')
    date=fields.Date(string='Date')
