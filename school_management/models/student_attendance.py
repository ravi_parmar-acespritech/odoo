# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields

class StudentAttendance(models.Model):
    _name = 'student.attendance'
    _description = 'student attendance data'

    name = fields.Char(string='Name')
    image = fields.Binary(string='Image')
    student_name = fields.Char(string='Student Name')
    regno = fields.Char(string='Reg No')
    roll_no = fields.Integer(string='Roll No')
    present = fields.Boolean(string='Present')
    late = fields.Boolean(string='Late')
    absent = fields.Boolean(string='Absent')
    half_day = fields.Boolean(string='HalfDay')


