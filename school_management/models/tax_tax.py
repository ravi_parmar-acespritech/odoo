# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields

class Tax(models.Model):
    _name = 'tax.tax'
    _description = 'tax data'

    name = fields.Char(string='Name')
    amount=fields.Float(string='Amount')




