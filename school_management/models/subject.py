# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields

class Subject(models.Model):
    _name = 'subject.subject'
    _description = 'student subject information'

    name = fields.Char(string='name')
    name_id = fields.Many2many('student.student')
    image = fields.Binary(string='Image')
    student_id =fields.Many2one('student.student',string='Student Id')
    subject =fields.Char(string='Subject')
    subject_type=fields.Selection([('type1', 'Theory'),('type2', 'practical')], 'Subject Type', default='type1')

