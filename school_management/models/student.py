# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields

class student(models.Model):
    _name = 'student.student'
    _description = 'student information'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirmed',),('done', 'Done',),('cancelled', 'Cancelled',)],default='draft')
    name = fields.Char(string='Student Name',required="1",help='student name')
    image = fields.Binary(string='Image')
    school_name = fields.Char(string='school Name',required=True,index=True,default='Gujarat Vidyapith')
    school_id = fields.Many2one('subject.subject', string='Subject', help="this is fields use subject information")
    address = fields.Text(string='Student Address')
    add_info = fields.Char(string='Address Info')
    roll_no = fields.Integer(string='Roll No',deflaut=10)
    roll_no_id =fields.One2many('subject.subject','student_id',string="Student Roll NO ")
    registration_date = fields.Date(string=' Registration Date')
    dob = fields.Date(string='Date of Birth')
    caste = fields.Selection([('type1', 'OBC'), ('type2', 'ST'), ('type3', 'SC'),('type4','General')],'Caste', default='type1')
    school_free = fields.Float(string='School Free')
    student_active = fields.Boolean(string='Student Active')
    course_id = fields.Selection([('type1', 'BCA'), ('type2', 'MCA'),('TYPE3','BCom'),('type4','BE'),('type4','Phd')], 'Course', default='type1')
    year_cleared = fields.Date(string='Year cleared')
    grade = fields.Boolean(string='Grade%')
    subject = fields.Char(string='Subject')
    enter_name = fields.Char(string='Enter Name')
    work_email = fields.Char(string=' work Email')
    Enter_contact = fields.Integer(string='Enter Contact')
    city = fields.Char(string="City")
    stream = fields.Char(string="Stream")
    user_id = fields.Many2one('res.users',string='user')




    def school_object(self):
        school = self.env.ref('school_management.school_information_view').read()[0]
        return school

    def Create_Invoice(self):
        self.state='confirm'

    def send_Email(self):
        self.state = 'done'

    def cancel(self):
        self.address=False
        self.school_free=False
        self.state = 'cancelled'


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    is_sale = fields.Boolean(string='Sale')
    address = fields.Char(string='Student Address')

#
class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"
    inventory =fields.Char(string='Inventory')


class AccountInvoice(models.Model):

    _inherit = 'account.move.line'
    inventory = fields.Char(string='Inventory')


class StockPicking(models.Model):
    _inherit = "stock.picking"
    stock = fields.Float(string='stock')


class StockMove(models.Model):
    _inherit = 'stock.move'
    product_name = fields.Char(string='product name')


class ResPartner(models.Model):
    _inherit = "res.partner"
    college_name = fields.Char(string='College Name')





