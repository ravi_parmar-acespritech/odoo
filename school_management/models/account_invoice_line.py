# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields,api

class AccountInvoiceLine(models.Model):
    _name ='account.invoice.line'
    _description = 'account invoice line info'


    name= fields.Char(string='name')
    invoice_id=fields.Many2one('account.invoice',string='Name')
    product_id = fields.Many2one('product.product',string='Product ID')
    qty = fields.Float(string='Qty')
    subtotal = fields.Integer(string='Subtotal')
    tax_ids = fields.Many2many('tax.tax',string='Tax')
    total = fields.Char(string='Total', compute="_compute_total")


    @api.onchange('invoice_id')
    def onchange_invoice_id(self):
        if self.invoice_id:
            if self.invoice_id.product_id:
                self.product_id=self.invoice_id.product_id.id


    @api.depends('subtotal', 'qty')
    def _compute_total(self):
        print("\n\ncall compute>>>>>>>>>>>")
        for record in self:
            record.total = record.subtotal*record.qty


