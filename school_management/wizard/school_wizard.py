# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class SchoolInformation(models.TransientModel):
    _name = "school.information"
    _description = "school Information Report"


    name = fields.Char(string="Name")
    city = fields.Char(string="City")
    address = fields.Text(string="Address")
    college = fields.Char(string="College")
    stream = fields.Char(string="Stream")

    def data_save(self):
        print('save')



