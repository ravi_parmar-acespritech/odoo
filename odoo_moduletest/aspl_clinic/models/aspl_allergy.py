# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import models,fields

class AsplAllergy(models.Model):
    _name = 'aspl.allergy'
    _description = 'allergy information'

    name = fields.Char(string='Name')