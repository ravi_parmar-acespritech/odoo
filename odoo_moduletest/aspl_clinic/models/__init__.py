# -*- coding: utf-8 -*-

from . import  aspl_medicine
from . import  aspl_allergy
from . import aspl_id_cards
from . import  aspl_id_cards_line
from . import aspl_appointment
from . import aspl_visit
from . import res_partner