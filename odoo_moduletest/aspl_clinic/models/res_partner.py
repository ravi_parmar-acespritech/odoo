# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing det


from  odoo import models,fields


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    is_patient = fields.Boolean(string='Is Patient')
    is_doctor = fields.Boolean(string='Is Doctor')
