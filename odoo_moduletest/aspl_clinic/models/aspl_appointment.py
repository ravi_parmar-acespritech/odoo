# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields,api

from datetime import date, datetime
from dateutil.relativedelta import relativedelta

class AsplAppointment(models.Model):
    _name = 'aspl.appointment'
    _description='patient appointment information'

    state = fields.Selection([('default', 'new'), ('confirm', 'Confirm'),('cancel', 'Cancel',)],default='default')
    name = fields.Char(string='Name')
    patient_id = fields.Many2one('res.partner',string='Patient')
    birthday = fields.Date(string='Date of Birth')
    age = fields.Integer(string="Age")
    mobile = fields.Char(string='Mobile')

    def appointment_confirm(self):
        self.state = 'confirm'

    def appointment_cancel(self):
        self.state = 'cancel'

    @api.onchange('birthday')
    def _onchange_birth_date(self):

        if self.birthday:
            d1 = datetime.strptime(str(self.birthday), "%Y-%m-%d").date()

            d2 = date.today()

            self.age = relativedelta(d2, d1).years


    def aspl_clinic(self):
        clinic = self.env.ref('aspl_clinic.clinic_wizard_act_views').read()[0]
        return clinic

























