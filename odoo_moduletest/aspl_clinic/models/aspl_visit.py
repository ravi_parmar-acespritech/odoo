# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import models,fields,api
import datetime


class AsplVisit(models.Model):
    _name = 'aspl.visit'
    _description = 'patient visit information'
    _rec_name = 'name'

    state = fields.Selection([('default', 'draft'), ('waiting', 'Waiting'),('progress', 'In Progress'),('complete', 'Complete'),('cancel','Cancel')],default='default')
    sequence = fields.Char(string="Service Number", readonly=True, copy=False, default='New')
    date = fields.Datetime(string='Date')
    name = fields.Char(string='Name')
    patient_id =fields.Many2one('res.partner',string='Patient')
    start_time=fields.Datetime(string="Start Time")
    complete_time=fields.Datetime(string='Complete Time')
    dob =fields.Date(string='Date of Birth')
    doctor=fields.Many2one('res.partner',string='Doctor')
    notes = fields.Text(string='Notes')
    medicine_lines =fields.One2many('aspl.medicine','medicine_id',string='Medicine Line')
    allergies=fields.Many2many('aspl.allergy',string='Allergies')
    identities_id = fields.One2many('card.line','identities_ids',string='Identities Id')

    @api.model
    def create(self, vals):
        vals['sequence'] = self.env['ir.sequence'].next_by_code('aspl.visit')
        print()
        return super(AsplVisit, self).create(vals)


    _sql_constraints = [
        ('name', 'unique (name)', 'name is already exists!')
    ]

    def arrived(self):
        self.state ='waiting'

    def start(self):
        self.state ='progress'
        self.date = datetime.datetime.now()

    def complete(self):
        self.state='complete'

    def cancel(self):
        self.state='cancel'








