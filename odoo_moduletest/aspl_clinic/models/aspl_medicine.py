# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import models,fields,api

class AsplMedicine(models.Model):
    _name = 'aspl.medicine'
    _description = 'medicine information'
    _rec_name = 'name'

    name=fields.Char(string='Name')
    product_id = fields.Many2one('product.product',string='Product')
    medicine_id= fields.Many2one('aspl.visit',string='Medicine')
    price=fields.Integer(string='Price')
    qty=fields.Float(string='Qty')
    subtotal = fields.Integer(string='Subtotal',compute='_compute_total')

    @api.depends('price','qty')
    def _compute_total(self):
        for record in self:
            record.subtotal = record.price * record.qty


    @api.model
    def create(self, vals):
        print("\n\n\n\n\n\n\n\n vals",vals)
        return super(AsplMedicine,self).create(vals)
