# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import models,fields

class AsplCardsLine(models.Model):
    _name = 'card.line'
    _description = 'card for patient'
    _rec_name = 'name'


    name=fields.Char(string='Name')
    id_card=fields.Many2one('id.card',string='Id Cards')
    code=fields.Char(string='Code')
    identities_ids= fields.Many2one('aspl.visit',string='Identities Ids')