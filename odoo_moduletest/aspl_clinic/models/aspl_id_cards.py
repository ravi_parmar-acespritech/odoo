# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import models,fields


class AsplIdCard(models.Model):
    _name = 'id.card'
    _description = 'id card information'

    name = fields.Char(string='Name')